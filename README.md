# Weather App

Example forecast application implemented with React technology, styled with React Styled Component, using Open Weather API.

## Live Demo

<a href = "https://weather-app-by-maryna.netlify.app" target= "_blank">LIVE DEMO</a>

## Libraries and Technologies

👉 Uses Open Weather API for weather data<br/>
👉 Uses React, including React Hooks<br/>
👉 Uses Axios for data fetching <br />
👉 Uses Local Storage to save dark theme & current temperature Conversion/celsius or fahrenheit/.<br />
👉 Styled with Styled Components<br/>

## Features

👉 Dark and Light Theme <br />
👉 City Finder <br />
👉 Geolocation API <br />
👉 Temperature Conversion: Celsius or Fahrenheit <br />

## Available Scripts

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.