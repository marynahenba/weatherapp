import React from 'react';
import moment from 'moment-timezone';
import { CurrentDayWeatherStyled } from '../styles/CurrentDayWeather.styled';
import { convertToF } from '../../helpers/convertDegree';

export default function CurrentDayWeather({
	location,
	data,
	tempUnit,
	timezone,
	nightMode,
}) {
	return (
		<CurrentDayWeatherStyled>
			{data && (
				<div className='wrapper'>
					<div className='content-box'>
						<h2 className='title'>{location}</h2>
						<img
							src={`images/${data.weather[0].icon}.png`}
							alt='Weather'
							className='icon'
						/>
						<p className='temp'>
							{tempUnit
								? convertToF(data.temp).toFixed(0)
								: Math.round(data.temp)}
							<span>{tempUnit ? '°F' : '°C'}</span>{' '}
						</p>
					</div>
					<div className='inner'>
						<div className='additional-info'>
							<p className='date'>
								<span className='day'>
									{moment.tz(data.dt * 1000, timezone).format('dddd')}
								</span>
								<span className='time'>
									{moment.tz(data.dt * 1000, timezone).format('h:mm A')}
								</span>
							</p>
						</div>
						<div className='additional-info'>
							<img
								src={
									nightMode
										? '/images/termometr-dark.png'
										: '/images/termometr-light.png'
								}
								alt='Termometr icon'
							/>
							<p className='feelsLike'>
								Feels like{' '}
								<span className='value'>
									{tempUnit
										? convertToF(data.feels_like).toFixed(0)
										: Math.round(data.feels_like)}
								</span>
								<span>{tempUnit ? '°F' : '°C'}</span>
							</p>
						</div>
						<div className='additional-info'>
							<img
								src={
									nightMode
										? '/images/cloud-dark.png'
										: '/images/cloud-light.png'
								}
								alt='Clouds icon'
							/>
							<p className='Clouds'>
								Cloudy{' '}
								<span className='value'>{`${Math.round(data.clouds)}%`}</span>
							</p>
						</div>
					</div>
				</div>
			)}
		</CurrentDayWeatherStyled>
	);
}
