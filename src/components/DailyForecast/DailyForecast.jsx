import React, { useState } from 'react';
import moment from 'moment-timezone';
import { convertToF } from '../../helpers/convertDegree';
import { ForecastStyled } from '../styles/ForecastStyled.styled';
import { UpcomingForecastItemStyled } from '../styles/UpcomingForecastItem.styled';

export default function DailyForecast({
	data,
	tempUnit,
	timezone,
	setDayDetails,
}) {
	const [selectedDay, setSelectedDay] = useState('');

	const showDayDetails = (day) => {
		setDayDetails(day);
	};

	return (
		<ForecastStyled>
			{data &&
				data.map((item) => {
					return (
						<UpcomingForecastItemStyled
							key={item.dt}
							onClick={() => {
								setSelectedDay(item);
								showDayDetails(item);
							}}
							className={
								selectedDay === item ? 'activeDay' : 'dailyWeatherItem'
							}
						>
							<h6 className='title'>
								{moment.tz(item.dt * 1000, timezone).format('ddd')}
							</h6>
							<img
								className='img'
								src={`images/${item.weather[0].icon}.png`}
								alt='Weather'
							/>
							<p>
								{tempUnit
									? `${convertToF(item.temp.min).toFixed(0)}°F - ${convertToF(
											item.temp.max
									  ).toFixed(0)}°F`
									: `${Math.round(item.temp.min)}°C - ${Math.round(
											item.temp.max
									  )}°C`}
							</p>
						</UpcomingForecastItemStyled>
					);
				})}
		</ForecastStyled>
	);
}
