import React from 'react';
import moment from 'moment-timezone';
import { convertToF } from '../../helpers/convertDegree';
import { ForecastStyled } from '../styles/ForecastStyled.styled';
import { UpcomingForecastItemStyled } from '../styles/UpcomingForecastItem.styled';

export default function HourlyForecast({ data, tempUnit, timezone }) {
	const forecast24Hours = data.slice(1, 25);
	return (
		<ForecastStyled>
			{forecast24Hours &&
				forecast24Hours.map((item) => {
					return (
						<UpcomingForecastItemStyled key={item.dt}>
							<h6 className='title'>
								{moment.tz(item.dt * 1000, timezone).format('LT')}
							</h6>
							<img
								className='img'
								src={`images/${item.weather[0].icon}.png`}
								alt='Weather'
							/>
							<p>
								{tempUnit
									? `${convertToF(item.temp).toFixed(0)}°F`
									: `${Math.round(item.temp)}°C`}
							</p>
						</UpcomingForecastItemStyled>
					);
				})}
		</ForecastStyled>
	);
}
