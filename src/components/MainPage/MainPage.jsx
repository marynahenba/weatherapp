import React, { useState, useEffect } from 'react';
import { useNightMode } from '../../hooks/useNightMode';
import { useTempUnit } from '../../hooks/useTempUnit';
import { getForecastData } from '../../services/getForecastData';
import {
	getCityNameByLocation,
	getLocationData,
} from '../../services/getLocationData';

import SearchBar from '../SearchBar/SearchBar';
import CurrentDayWeather from '../CurrentDayWeather/CurrentDayWeather';
import CurrentDayDetails from '../CurrentDayDetails/CurrentDayDetails';
import UpcomingForecast from '../UpcomingForecast/UpcomingForecast';
import Loader from '../Loader/Loader';
import ErrorLoader from '../ErrorLoader/ErrorLoader';

import { GlobalStyles } from '../styles/Global';
import { Container } from '../styles/MainPage.styled';
import { ThemeProvider } from 'styled-components';
import { dark, light } from '../styles/Theme.styled';

export default function MainPage() {
	const [search, setSearch] = useState('');
	const [location, setLocation] = useState('');
	const [timezone, setTimezone] = useState('');
	const [forecast, setForecast] = useState('');
	const [dayDetails, setDayDetails] = useState('');
	const [upcomingForecast, setUpcomingForecast] = useState('');
	const [searchSuggestions, setSearchSuggestions] = useState([]);
	const [showDays, setShowDays] = useState(false);
	const [errorLoader, setErrorLoader] = useState(false);
	const [nightMode, nightModeChanged] = useNightMode();
	const [unitMode, unitModeChanged] = useTempUnit();

	useEffect(() => {
		const getCurrentLocation = (pos) => {
			getCityNameByLocation(pos.coords.latitude, pos.coords.longitude).then(
				({ data }) => {
					const city = data[0].name;
					const state = data[0].state ? data[0].state : '';
					const currentCityName = `${city} ${state}`;
					setLocation(currentCityName);
					getForecastData(pos.coords.latitude, pos.coords.longitude).then(
						({ data }) => {
							setForecast(data.current);
							setDayDetails(data.current);
							setTimezone(data.timezone);
							setUpcomingForecast(data);
						}
					);
				}
			);
		};
		const error = () => {
			setErrorLoader(true);
		};

		navigator.geolocation.getCurrentPosition(getCurrentLocation, error);
	}, []);

	useEffect(() => {
		if (search.length > 0) {
			getLocationData(search).then(({ data }) => {
				setSearchSuggestions(data);
			});
		} else {
			setSearchSuggestions([]);
		}
	}, [search]);

	const onCitySelected = (lat, lon) => {
		getForecastData(lat, lon)
			.then(({ data }) => {
				setForecast(data.current);
				setDayDetails(data.current);
				setTimezone(data.timezone);
				setUpcomingForecast(data);
			})
			.then(() => {
				setSearchSuggestions([]);
				setSearch('');
			});
	};

	const handleClick = (e) => {
		const lat = e.target.getAttribute('data-lat');
		const lon = e.target.getAttribute('data-lon');
		const locationName = e.target.getAttribute('locationname');
		setLocation(locationName);
		onCitySelected(lat, lon);
	};

	const nightModeCallback = () => {
		nightModeChanged();
	};

	const setShowDaysCallback = (enabled) => {
		setShowDays(enabled);
	};

	const unitTempCallback = (enabled) => {
		unitModeChanged(enabled);
	};

	return (
		<ThemeProvider theme={nightMode ? dark : light}>
			<GlobalStyles />
			<Container>
				<SearchBar
					search={search}
					setSearch={setSearch}
					searchSuggestions={searchSuggestions}
					nightMode={nightMode}
					nightModeCallback={nightModeCallback}
					unitMode={unitMode}
					unitTempCallback={unitTempCallback}
					onClick={handleClick}
				/>
				{forecast ? (
					<div className='content-wrapper'>
						<CurrentDayWeather
							location={location}
							data={forecast}
							tempUnit={unitMode}
							nightMode={nightMode}
							timezone={timezone}
						/>
						<div className='content-inner'>
							<UpcomingForecast
								data={upcomingForecast}
								showDays={showDays}
								setShowDaysCallback={setShowDaysCallback}
								unitMode={unitMode}
								unitTempCallback={unitTempCallback}
								timezone={timezone}
								setDayDetails={setDayDetails}
							/>
							<CurrentDayDetails data={dayDetails} timezone={timezone} />
						</div>
					</div>
				) : (
					<ThemeProvider theme={nightMode ? dark : light}>
						{!errorLoader ? <Loader /> : <ErrorLoader />}
						<GlobalStyles />
					</ThemeProvider>
				)}
			</Container>
		</ThemeProvider>
	);
}
