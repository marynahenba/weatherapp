import React from 'react';
import moment from 'moment-timezone';

import { CurrentDayDetailsStyled } from '../styles/CurrentDayDetails.styled';

export default function CurrentDayDetails({ data, timezone }) {
	return (
		<CurrentDayDetailsStyled>
			{data && (
				<div className='wrapper'>
					<div className='card'>
						<h4 className='title'>UV index</h4>
						<div className='description'>
							<img src='/images/umbrella.png' alt='UVI icon' />
							{Math.round(data.uvi)}
							<span className='unit'></span>
						</div>
					</div>
					<div className='card'>
						<h4 className='title'>Humidity</h4>
						<div className='description'>
							<img src='/images/humidity.png' alt='Humiidity icon' />
							{data.humidity}
							<span className='unit'>%</span>
						</div>
					</div>
					<div className='card'>
						<h4 className='title'>Pressure</h4>
						<div className='description'>
							<img src='/images/compass.png' alt='Pressure icon' />
							{data.pressure}
							<span className='unit'>hPa</span>
						</div>
					</div>
					<div className='card'>
						<h4 className='title'>Wind</h4>
						<div className='description'>
							<img src='/images/wind.png' alt='Wind icon' />
							{Math.round(data.wind_speed)}
							<span className='unit'>km/h</span>
						</div>
					</div>
					<div className='card'>
						<h4 className='title'>Visibility</h4>
						{data.visibility ? (
							<div className='description'>
								<img src='/images/50n.png' alt='Visibility icon' />
								{Math.round(data.visibility) / 1000}
								<span className='unit'>km</span>
							</div>
						) : (
							<p className='description'>?</p>
						)}
					</div>
					<div className='card'>
						<h4 className='title'>Sunrise/Sunset</h4>
						<div className='description'>
							<img src='/images/sunrise.png' alt='Sunrise icon' />
							{moment.tz(data.sunrise * 1000, timezone).format('LT')}
						</div>
						<div className='description'>
							<img src='/images/sunset.png' alt='Sunset icon' />
							{moment.tz(data.sunset * 1000, timezone).format('LT')}
						</div>
					</div>
				</div>
			)}
		</CurrentDayDetailsStyled>
	);
}
