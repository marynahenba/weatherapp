import React from 'react';
import { SearchBarStyled, ThemeTumbler } from '../styles/SearchBar.styled';

export default function SearchBar({
	search,
	searchSuggestions,
	setSearch,
	nightMode,
	nightModeCallback,
	onClick,
}) {
	const handleChange = (e) => {
		setSearch(e.target.value);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
	};

	return (
		<SearchBarStyled>
			<form onSubmit={handleSubmit} className='form'>
				<input
					className='input'
					type='text'
					placeholder='Type your city...'
					value={search}
					minLength={2}
					maxLength={50}
					onChange={handleChange}
				/>
				<ul className='list'>
					{searchSuggestions.map((item) => {
						const state = item.state !== undefined ? `, ${item.state}` : '';
						return (
							<li
								className='list-item'
								key={item.lon}
								data-lat={item.lat}
								data-lon={item.lon}
								locationname={`${item.name}${state}`}
								onClick={onClick}
							>
								{`[${item.country}], ${item.name}`}
								{item.state && `, ${item.state}`}
							</li>
						);
					})}
				</ul>
			</form>
			<ThemeTumbler>
				<input
					className='checkbox'
					type='checkbox'
					id='checkbox'
					checked={nightMode}
					onChange={nightModeCallback}
				/>
				<label className='label' htmlFor='checkbox'>
					<div className='sun'>
						<svg viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
							<title />
							<circle cx='12' cy='12' fill='#d9d9d9' r='5' />
							<path
								d='M21,13H20a1,1,0,0,1,0-2h1a1,1,0,0,1,0,2Z'
								fill='#464646'
							/>
							<path d='M4,13H3a1,1,0,0,1,0-2H4a1,1,0,0,1,0,2Z' fill='#464646' />
							<path
								d='M17.66,7.34A1,1,0,0,1,17,7.05a1,1,0,0,1,0-1.41l.71-.71a1,1,0,1,1,1.41,1.41l-.71.71A1,1,0,0,1,17.66,7.34Z'
								fill='#464646'
							/>
							<path
								d='M5.64,19.36a1,1,0,0,1-.71-.29,1,1,0,0,1,0-1.41L5.64,17a1,1,0,0,1,1.41,1.41l-.71.71A1,1,0,0,1,5.64,19.36Z'
								fill='#464646'
							/>
							<path
								d='M12,5a1,1,0,0,1-1-1V3a1,1,0,0,1,2,0V4A1,1,0,0,1,12,5Z'
								fill='#464646'
							/>
							<path
								d='M12,22a1,1,0,0,1-1-1V20a1,1,0,0,1,2,0v1A1,1,0,0,1,12,22Z'
								fill='#464646'
							/>
							<path
								d='M6.34,7.34a1,1,0,0,1-.7-.29l-.71-.71A1,1,0,0,1,6.34,4.93l.71.71a1,1,0,0,1,0,1.41A1,1,0,0,1,6.34,7.34Z'
								fill='#464646'
							/>
							<path
								d='M18.36,19.36a1,1,0,0,1-.7-.29L17,18.36A1,1,0,0,1,18.36,17l.71.71a1,1,0,0,1,0,1.41A1,1,0,0,1,18.36,19.36Z'
								fill='#464646'
							/>
						</svg>
					</div>
					<div className='ball'></div>
					<div className='moon'>
						<svg viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
							<title />
							<path
								d='M20.21,15.32A8.56,8.56,0,1,1,11.29,3.5a.5.5,0,0,1,.51.28.49.49,0,0,1-.09.57A6.46,6.46,0,0,0,9.8,9a6.57,6.57,0,0,0,9.71,5.72.52.52,0,0,1,.58.07A.52.52,0,0,1,20.21,15.32Z'
								fill='black'
							/>
						</svg>
					</div>
				</label>
			</ThemeTumbler>
		</SearchBarStyled>
	);
}
