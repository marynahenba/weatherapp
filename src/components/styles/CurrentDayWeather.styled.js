import styled from 'styled-components';

export const CurrentDayWeatherStyled = styled.div`
	.wrapper {
		width: 300px;
		margin: 0 15px 0 0;
	}

	.inner {
		width: 100%;
		background-color: ${({ theme }) => theme.colors.bgColorItems};
		border-radius: 25px;
		padding: 20px;
	}

	.title {
		font-size: 2rem;
		font-weight: 500;
		margin-bottom: 20px;
		text-align: left;
	}

	.icon {
		display: block;
		width: 200px;
		height: 200px;
		margin: 0 auto 15px;
	}

	.temp {
		font-size: 5rem;
		text-align: right;
		margin-bottom: 30px;
		span {
			font-size: 1.8rem;
		}
	}

	.additional-info {
		display: flex;
		align-items: center;
		margin-bottom: 15px;
		img {
			width: 30px;
			margin-right: 10px;
		}
	}

	.date {
		font-size: 1.2rem;
		margin-bottom: 10px;
	}

	.day {
		font-weight: 500;
		margin-right: 10px;
	}

	.time,
	.value {
		font-weight: 300;
	}

	@media (max-width: 935px) {
		width: 100%;

		.wrapper {
			width: 100%;
			display: flex;
			align-items: flex-end;
			justify-content: center;
			margin-bottom: 50px;
		}

		.temp {
			text-align: center;
			margin: 0;
		}

		.content-box {
			margin-right: 30px;
		}

		.inner {
			width: 300px;
			height: auto;
		}
	}

	@media (max-width: 675px) {
		.wrapper {
			flex-direction: column;
			justify-content: center;
			align-items: center;
		}

		.inner {
			display: none;
		}

		.title {
			margin-bottom: 30px;
		}

		.temp {
			text-align: right;
		}
	}
`;
