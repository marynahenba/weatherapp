import styled from 'styled-components';

export const UpcomingForecastItemStyled = styled.div`
	text-align: center;
	min-width: 160px;
	height: 100%;
	background-color: ${({ theme }) => theme.colors.bgColorUpcomingItem};
	border: 2px solid ${({ theme }) => theme.colors.borderColor};
	border-radius: 15px;
	padding: 10px 0;
	margin: 5px;

	.title {
		font-size: 1rem;
	}

	.img {
		max-width: 90px;
		height: 90px;
	}

	@media (max-width: 675px) {
		min-width: 130px;

		.img {
			max-width: 70px;
			height: 70px;
		}
	}

	@media (max-width: 450px) {
			min-width: 110px;

		.img {
			max-width: 50px;
			height: 50px;
		}
	}
	}
`;
