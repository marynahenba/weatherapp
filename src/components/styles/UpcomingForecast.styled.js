import styled from 'styled-components';

export const UpcomingForecastStyled = styled.div`
	.wrapper {
		padding: 10px;
		margin: 0 auto 20px;
		border-radius: 25px;
		background-color: ${({ theme }) => theme.colors.bgColorItems};
		overflow: hidden;
		border-radius: 25px;
	}

	.btnWrapper {
		margin: 10px 15px 20px 15px;
		display: flex;
		justify-content: space-between;
	}

	.btn-week,
	.btn-today,
	.btn-celsius,
	.btn-fahrenheit {
		border: 1px solid ${({ theme }) => theme.colors.borderColor};
		border-radius: 15px;
		padding: 5px 15px;
		cursor: pointer;
		background-color: ${({ theme }) => theme.colors.bgBtn};
		color: ${({ theme }) => theme.colors.fontBtnColor};
		font-size: 1.1rem;
	}

	.btn-week,
	.btn-celsius {
		margin-right: 10px;
	}

	.active {
		border: 1px solid transparent;
		background-color: ${({ theme }) => theme.colors.bgBtnActive};
		color: ${({ theme }) => theme.colors.fontBtnColorActive};
	}

	.activeDay {
		border: 2px solid ${({ theme }) => theme.colors.borderColorActiveDay};
		cursor: pointer;
	}

	.dailyWeatherItem {
		cursor: pointer;
	}

	@media (max-width: 675px) {
		.wrapper {
			padding: 10px;
		}
	}

	@media (max-width: 450px) {
		.btn-week,
		.btn-today,
		.btn-celsius,
		.btn-fahrenheit {
			font-size: 0.9rem;
		}
	}
`;
