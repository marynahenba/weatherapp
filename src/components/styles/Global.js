import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
import url('https://fonts.googleapis.com/css?family=Montserrat:200,300,400,700,900&display=swap');

*,
*::before,
*::after {
	margin: 0;
	padding: 0;
    box-sizing: border-box;
}

ul {
	list-style: none;
	margin: 0;
	padding: 0;
}

body {
	width: 100vw;
	height: 100vh;
	font-family: 'Montserrat', sans-serif;
	background-color: ${({ theme }) => theme.colors.bgColor};
	color: ${({ theme }) => theme.colors.mainTextColor};
}
`;
