import styled from 'styled-components';

export const ForecastStyled = styled.div`
	display: flex;
	align-items: center;
	white-space: nowrap;
	overflow-x: scroll;
	box-sizing: content-box;
	border-radius: 10px;
	margin: 10px;
	-ms-overflow-style: none;
	scrollbar-width: none;

	::-webkit-scrollbar {
		display: none;
	}

	@media (max-width: 450px) {
		margin: 0;
	}
`;
