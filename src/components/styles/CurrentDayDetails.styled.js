import styled from 'styled-components';

export const CurrentDayDetailsStyled = styled.div`
	.wrapper {
		width: 100%;
		display: flex;
		flex-wrap: wrap;
		justify-content: space-evenly;
		grid-gap: 10px;
	}

	.card {
		width: 200px;
		height: 150px;
		background-color: ${({ theme }) => theme.colors.bgColorItems};
		border-radius: 25px;
		margin-bottom: 15px;
		padding: 20px;
		flex-grow: 1;
		flex-basis: 30%;
	}

	.title {
		font-size: 1.1rem;
		font-weight: 300;
		margin-bottom: 20px;
	}

	.description {
		display: flex;
		align-items: flex-end;
		text-align: center;
		font-size: 1.5rem;
		font-weight: 500;
	}

	.unit {
		font-size: 1.1rem;
	}

	img {
		width: 30px;
		height: 30px;
		margin-right: 10px;
	}

	@media (max-width: 675px) {
		.card {
			flex-basis: 35%;
		}
	}

	@media (max-width: 450px) {
		.card {
			width: 180px;
			height: 130px;
		}
	}

	@media (max-width: 390px) {
		.card {
			flex-basis: 50%;
			margin-bottom: 0;
		}
	}
`;
