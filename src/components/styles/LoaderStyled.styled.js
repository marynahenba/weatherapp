import styled from 'styled-components';

export const LoaderStyled = styled.div`
	opacity: 1;
	position: absolute;
	margin-left: -55px;
	margin-top: -70px;
	height: 110px;
	width: 110px;
	left: 50%;
	top: 50%;

	svg {
		width: 110px;
		height: 110px;
	}

	path {
		stroke: #9ea1a4;
		stroke-width: 0.25;
	}

	#cloud {
		position: relative;
		z-index: 2;
	}

	#cloud path {
		fill: #efefef;
	}

	#sun {
		margin-left: -10px;
		margin-top: 6px;
		opacity: 0;
		width: 60px;
		height: 60px;
		position: absolute;
		left: 45px;
		top: 15px;
		z-index: 1;
		animation-name: rotate;
		animation-duration: 16000ms;
		animation-iteration-count: infinite;
		animation-timing-function: linear;
	}

	#sun path {
		stroke: #fdd020;
		stroke-width: 0.18;
		fill: #9ea1a4;
	}

	@keyframes rotate {
		0% {
			transform: rotateZ(0deg);
		}

		100% {
			transform: rotateZ(360deg);
		}
	}

	.text {
		letter-spacing: 1px;
		text-align: center;
		margin-left: -43px;
		font-weight: bold;
		font-size: 11px;
		color: ${({ theme }) => theme.colors.mainTextColor};
		width: 200px;
	}
`;
