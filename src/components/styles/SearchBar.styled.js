import styled from 'styled-components';

export const SearchBarStyled = styled.div`
	display: flex;
	align-items: center;
	margin: 0 auto 40px;

	.form {
		display: block;
		width: 500px;
		position: relative;
		margin: 0 auto;
		z-index: 3;
	}

	.input {
		width: 100%;
		border-radius: 25px;
		border: 1px solid ${({ theme }) => theme.colors.inputBorderColor};
		padding: 10px 20px;
		background-color: ${({ theme }) => theme.colors.bgInput};
		color: ${({ theme }) => theme.colors.mainTextColor};
		font-size: 1.1rem;
	}

	.list {
		position: absolute;
		top: 45px;
		left: 0;
		right: 0;
		z-index: 3;
	}

	.list-item {
		padding: 10px 20px;
		border-radius: 25px;
		margin-bottom: 2px;
		background-color: ${({ theme }) => theme.colors.bgColorSearchCard};
		border: 1px solid ${({ theme }) => theme.colors.borderSearchCard};
	}

	@media (max-width: 675px) {
		.form {
			width: 270px;
		}

		.input {
			padding: 7px 10px;
		}
	}

	@media (max-width: 390px) {
		.form {
			width: 200px;
		}
	}
`;

export const ThemeTumbler = styled.div`
	.checkbox {
		opacity: 0;
		position: absolute;
	}

	.label {
		display: flex;
		align-items: center;
		justify-content: space-between;
		background-color: ${({ theme }) => theme.colors.themeTumblerBg};
		border-radius: 50px;
		cursor: pointer;
		padding: 5px;
		position: relative;
		height: 40px;
		width: 80px;
	}
	.label .ball {
		background-color: ${({ theme }) => theme.colors.ballTumblerBg};
		border-radius: 50%;
		position: absolute;
		left: 20px;
		height: 30px;
		width: 30px;
		transform: translateX(-50%);
		transition: transform 0.2s;
	}

	.checkbox:checked + .label .ball {
		transform: translateX(24px);
	}
	.sun,
	.moon {
		padding-top: 4px;
	}
	.moon,
	.sun {
		svg {
			width: 30px;
			height: 30px;
			path {
				fill: ${({ theme }) => theme.colors.tumblerIconColor};
			}
		}
	}

	@media (max-width: 675px) {
		.label {
			width: 55px;
			height: 30px;
		}

		.label .ball {
			left: 15px;
			width: 20px;
			height: 20px;
		}

		.checkbox:checked + .label .ball {
			transform: translateX(15px);
		}

		.moon,
		.sun {
			svg {
				width: 20px;
				height: 20px;
			}
		}
	}
`;
