import styled from 'styled-components';

export const Container = styled.div`
	max-width: 100vw;
	padding: 20px 20px 10px;
	margin: 0 auto;

	.content-wrapper {
		width: 100%;
		display: flex;
		justify-content: space-between;
		align-items: flex-start;
	}

	.content-inner {
		width: calc(100% - 350px);
	}

	@media (max-width: 935px) {
		.content-wrapper {
			flex-direction: column;
		}

		.content-inner {
			width: 100%;
		}
	}
	@media (max-width: 675px) {
		.content-wrapper {
			flex-direction: column;
		}

		.content-inner {
			width: 100%;
		}
	}

	@media (max-width: 390px) {
		padding: 15px 15px 10px;
	}
`;
