import styled from 'styled-components';

export const ErrorLoaderStyled = styled.div`
	opacity: 1;
	position: absolute;
	margin-left: -55px;
	margin-top: -100px;
	height: 110px;
	width: 110px;
	left: 50%;
	top: 50%;

	svg {
		width: 110px;
		height: 110px;
	}

	path {
		stroke: #9ea1a4;
		stroke-width: 0.25;
	}

	#cloud {
		position: relative;
		z-index: 2;
	}

	#cloud path {
		fill: #efefef;
	}

	.rain {
		position: absolute;
		width: 70px;
		height: 70px;
		margin-top: -32px;
		margin-left: 19px;
	}

	.drop {
		opacity: 1;
		background: #9ea1a4;
		display: block;
		float: left;
		width: 3px;
		height: 5px;
		margin-left: 4px;
		border-radius: 0px 0px 6px 6px;
		animation-name: drop;
		animation-duration: 1200ms;
		animation-iteration-count: infinite;
	}

	.drop:nth-child(1) {
		animation-delay: -1300ms;
	}

	.drop:nth-child(2) {
		animation-delay: -2400ms;
	}

	.drop:nth-child(3) {
		animation-delay: -3900ms;
	}

	.drop:nth-child(4) {
		animation-delay: -5250ms;
	}

	.drop:nth-child(5) {
		animation-delay: -1400ms;
	}

	.drop:nth-child(6) {
		animation-delay: -7900ms;
	}

	.drop:nth-child(7) {
		animation-delay: -9000ms;
	}

	.drop:nth-child(8) {
		animation-delay: -1050ms;
	}

	.drop:nth-child(9) {
		animation-delay: -1130ms;
	}

	.drop:nth-child(10) {
		animation-delay: -13000ms;
	}

	@keyframes drop {
		50% {
			height: 35px;
			opacity: 0;
		}

		51% {
			opacity: 0;
		}

		100% {
			height: 1px;
			opacity: 0;
		}
	}

	.text {
		letter-spacing: 1px;
		text-align: center;
		margin-left: -43px;
		font-weight: bold;
		margin-top: 20px;
		font-size: 11px;
		color: ${({ theme }) => theme.colors.mainTextColor};
		width: 200px;
	}
`;
