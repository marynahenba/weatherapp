import React from 'react';
import DailyForecast from '../DailyForecast/DailyForecast';
import HourlyForecast from '../HourlyForecast/HourlyForecast';
import { UpcomingForecastStyled } from '../styles/UpcomingForecast.styled';

export default function UpcomingForecast({
	data,
	showDays,
	setShowDaysCallback,
	unitMode,
	unitTempCallback,
	timezone,
	setDayDetails,
}) {
	const switchUpcomingWeather = (enabled) => {
		setShowDaysCallback(enabled);
	};

	const changedTemp = (enabled) => {
		unitTempCallback(enabled);
	};

	return (
		<UpcomingForecastStyled>
			<div className='wrapper'>
				<div className={'btnWrapper'}>
					<div>
						<button
							className={`btn-week ${showDays ? '' : 'active'}`}
							onClick={() => switchUpcomingWeather(false)}
						>
							Week
						</button>
						<button
							className={`btn-today ${showDays ? 'active' : ''}`}
							onClick={() => {
								switchUpcomingWeather(true);
								setDayDetails(data.current);
							}}
						>
							Today
						</button>
					</div>
					<div>
						<button
							className={`btn-celsius ${unitMode ? '' : 'active'}`}
							onClick={() => changedTemp(false)}
						>
							°C
						</button>
						<button
							className={`btn-fahrenheit ${unitMode ? 'active' : ''}`}
							onClick={() => changedTemp(true)}
						>
							°F
						</button>
					</div>
				</div>
				{!showDays ? (
					<DailyForecast
						data={data.daily}
						tempUnit={unitMode}
						timezone={timezone}
						setDayDetails={setDayDetails}
					/>
				) : (
					<HourlyForecast
						data={data.hourly}
						tempUnit={unitMode}
						timezone={timezone}
					/>
				)}
			</div>
		</UpcomingForecastStyled>
	);
}
