import axios from 'axios';

const API_URL = '//api.openweathermap.org';

const getForecastData = async (lat, lon) => {
	try {
		const response = await axios.get(
			`${API_URL}/data/3.0/onecall?lat=${lat}&lon=${lon}&appid=a5bec385d6e0ab23604b3dbd28c8aed3&units=metric`
		);
		return response;
	} catch (err) {
		console.log(err);
	}
};

export { getForecastData };
