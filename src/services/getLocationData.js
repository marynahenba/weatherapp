import axios from 'axios';

const API_URL = '//api.openweathermap.org';

const getLocationData = async (location) => {
	try {
		const response = await axios.get(
			`${API_URL}/geo/1.0/direct?q=${location}&limit=5&appid=a5bec385d6e0ab23604b3dbd28c8aed3`
		);
		return response;
	} catch (err) {
		console.log(err);
	}
};

const getCityNameByLocation = async (lat, lon) => {
	try {
		const response = await axios.get(
			`${API_URL}/geo/1.0/reverse?lat=${lat}&lon=${lon}&limit=1&appid=a5bec385d6e0ab23604b3dbd28c8aed3`
		);
		return response;
	} catch (err) {
		console.log(err);
	}
};

export { getLocationData, getCityNameByLocation };
